using System;
using folhapagamento.Uteis;
namespace folhapagamento.Entities.Funcionario {
    public class Beneficios {
        public static double SalarioFamilia (int qtdDependentes, double renda) {
            double valorPorFilho = 0;
            if (qtdDependentes > 0) {
                if (renda < 859.89)
                    valorPorFilho = 44.09;
                if ((renda > 859.88) && (renda < 1292.44))
                    valorPorFilho = 31.07;
            }
            return valorPorFilho;
        }
        public static double DSR (double salarioHora, int mes = 0, int ano = 0) {
            if (mes == 0)
                mes = DateTimeOffset.UtcNow.Month;
            if (ano == 0)
                ano = DateTimeOffset.UtcNow.Year;
            double dsr = ((DataHora.ContaSabadoDomingo (mes, ano)) * 7.33) * salarioHora;
            return dsr;
        }
    }
}