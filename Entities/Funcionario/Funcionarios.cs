using System;
using folhapagamento.Entities.Funcionario;

namespace FolhaPagamento.Entities.Funcionario {
    public class Funcionarios {
        public Funcionarios (
            string nome,
            string sexo,
            string setor,
            int nroFilhoSalFamilia,
            int nroFilhosDescIR,
            int codSalario,
            int nroHorasTrabalhadasMes,
            int nroHorasExtrasDiaSemana,
            int nroHorasExtrasFimSemana,
            int nroHorasNoturnas
        ) {
            Nome = nome;
            Sexo = sexo;
            Setor = setor;
            NroFilhoSalFamilia = nroFilhoSalFamilia;
            NroFilhosDescIR = nroFilhosDescIR;
            CodSalario = codSalario;
            NroHorasTrabalhadasMes = nroHorasTrabalhadasMes;
            NroHorasExtrasDiaSemana = nroHorasExtrasDiaSemana;
            NroHorasExtrasFimSemana = nroHorasExtrasFimSemana;
            NroHorasNoturnas = nroHorasNoturnas;
            SalarioHora = Salario.CalcSalario (codSalario);
            SalarioMes = Salario.SalarioHoraParaSalarioMes (SalarioHora);
            SalarioFamilia = Beneficios.SalarioFamilia (nroFilhoSalFamilia, SalarioMes);
        }
        public void Listar () {
            Console.WriteLine ($"NOME: {Nome}");
            Console.WriteLine ($"SEXO: {Sexo}");
            Console.WriteLine ($"SETOR: {Setor}");
            Console.WriteLine ($"NRO DE FILHOS PARA SALÁRIO FAMÍLIA: {NroFilhoSalFamilia}");
            Console.WriteLine ($"NRO DE FILHOS PARA DESCONTO NO IR: {NroFilhosDescIR}");
            Console.WriteLine ($"CÓDIGO DO SALÁRIO: {CodSalario}");
            Console.WriteLine ($"NRO DE HORAS TRABALHADAS NO MÊS: {NroHorasTrabalhadasMes}");
            Console.WriteLine ($"NRO DE HORAS EXTRAS EM DIA DA SEMANA: {NroHorasExtrasDiaSemana}");
            Console.WriteLine ($"NRO DE HORAS EXTRAS EM FINAL DE SEMANA: {NroHorasExtrasFimSemana}");
            Console.WriteLine ($"NRO DE HORAS NOTURNAS (ADICIONAL NOTURNO): {NroHorasNoturnas}");
            Console.WriteLine ("===========================================");
            Console.WriteLine ($"VALOR DO SALÁRIO MENSAL: {SalarioMes}");
            Console.WriteLine ($"VALOR DO SALÁRIO POR HORA: {SalarioHora}");
            Console.WriteLine ($"VALOR DO SALÁRIO FAMILIA: {SalarioFamilia}");
            Console.WriteLine ("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            Console.WriteLine ("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
            Console.WriteLine ("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        }
        public string Nome { get; private set; }
        public string Sexo { get; private set; }
        public string Setor { get; private set; }
        public int NroFilhoSalFamilia { get; private set; }
        public int NroFilhosDescIR { get; private set; }
        public int CodSalario { get; private set; }
        public int NroHorasTrabalhadasMes { get; private set; }
        public int NroHorasExtrasDiaSemana { get; private set; }
        public int NroHorasExtrasFimSemana { get; private set; }
        public int NroHorasNoturnas { get; private set; }
        public double SalarioHora { get; set; }
        public double SalarioMes { get; set; }
        public double SalarioFamilia { get; set; }
    }
}