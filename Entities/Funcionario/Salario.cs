namespace FolhaPagamento.Entities.Funcionario {
    public class Salario {
        public static double CalcSalario (int cod) {
            if (cod == 1)
                return 12.00;
            if (cod == 2)
                return 15.00;
            if (cod == 3)
                return 17.00;
            if (cod == 4)
                return 25.00;
            if (cod == 5)
                return 50.00;
            return 0;
        }

        public static double SalarioMesParaSalarioHora (double SalarioMes) {
            return SalarioMes / 220;
        }
        public static double SalarioHoraParaSalarioMes (double SalarioHora) {
            return SalarioHora * 220;
        }
    }
}