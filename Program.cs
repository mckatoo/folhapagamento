﻿using System;
using FolhaPagamento.Entities.Funcionario;

namespace FolhaPagamento {
    class Program {
        static void Main (string[] args) {
            Funcionarios[] funcionarios = new Funcionarios[3];
            string Nome = "", Sexo = "", Setor = "";
            bool SexoValido = false;
            int NroFilhoSalFamilia = 0, NroFilhosDescIR = 0, CodSalario = 0, NroHorasExtrasDiaSemana = 0, NroHorasExtrasFimSemana = 0, NroHorasNoturnas = 0, NroHorasTrabalhadasMes = 0;
            void Limpar () {
                Nome = "";
                Sexo = "";
                Setor = "";
                SexoValido = false;
                NroFilhoSalFamilia = 0;
                NroFilhosDescIR = 0;
                CodSalario = 0;
                NroHorasExtrasDiaSemana = 0;
                NroHorasExtrasFimSemana = 0;
                NroHorasNoturnas = 0;
                NroHorasTrabalhadasMes = 0;
                SexoValido = false;
            }
            for (int i = 0; i < funcionarios.Length; i++) {
                while (Nome == "") {
                    Console.Write ("NOME: ");
                    Nome = Console.ReadLine ().ToUpper ();
                }

                while (SexoValido == false) {
                    Console.Write ("SEXO (M-MASCULINO / F-FEMININO): ");
                    Sexo = Console.ReadLine ().ToUpper ();
                    if (Sexo == "M" || Sexo == "F")
                        SexoValido = true;
                }

                while (Setor == "") {
                    Console.Write ("SETOR: ");
                    Setor = Console.ReadLine ().ToUpper ();
                }

                Console.Write ("NRO DE FILHOS PARA SALÁRIO FAMÍLIA: ");
                while (int.TryParse (Console.ReadLine (), out NroFilhoSalFamilia) == false) {
                    Console.Write ("NRO DE FILHOS PARA SALÁRIO FAMÍLIA: ");
                }

                Console.Write ("NRO DE FILHOS PARA DESCONTO EM IR: ");
                while (int.TryParse (Console.ReadLine (), out NroFilhosDescIR) == false) {
                    Console.Write ("NRO DE FILHOS PARA DESCONTO EM IR: ");
                }

                bool ValidaCodSalario () {
                    if (int.TryParse (Console.ReadLine (), out CodSalario) && (CodSalario > 0 && CodSalario < 6))
                        return true;
                    return false;
                }

                Console.Write ("CÓDIGO DO SALÁRIO: ");
                while (ValidaCodSalario () == false) {
                    Console.Write ("CÓDIGO DO SALÁRIO: ");
                }

                Console.Write ("NRO DE HORAS TRABALHADAS NO MÊS: ");
                while (int.TryParse (Console.ReadLine (), out NroHorasTrabalhadasMes) == false) {
                    Console.Write ("NRO DE HORAS TRABALHADAS NO MÊS: ");
                }

                Console.Write ("NRO DE HORAS EXTRAS EM DIA DA SEMANA: ");
                while (int.TryParse (Console.ReadLine (), out NroHorasExtrasDiaSemana) == false) {
                    Console.Write ("NRO DE HORAS EXTRAS EM DIA DA SEMANA: ");
                }

                Console.Write ("NRO DE HORAS EXTRAS EM FIM DE SEMANA: ");
                while (int.TryParse (Console.ReadLine (), out NroHorasExtrasFimSemana) == false) {
                    Console.Write ("NRO DE HORAS EXTRAS EM FIM DE SEMANA: ");
                }

                Console.Write ("NRO DE HORAS NOTURNAS: ");
                while (int.TryParse (Console.ReadLine (), out NroHorasNoturnas) == false) {
                    Console.Write ("NRO DE HORAS NOTURNAS: ");
                }

                funcionarios[i] = new Funcionarios (Nome, Sexo, Setor, NroFilhoSalFamilia, NroFilhosDescIR, CodSalario, NroHorasTrabalhadasMes, NroHorasExtrasDiaSemana, NroHorasExtrasFimSemana, NroHorasNoturnas);
                Limpar ();
            }
            Console.WriteLine ("APERTE ENTER PARA CONTINUAR...");
            Console.ReadKey ();
            Console.Clear ();

            foreach (var item in funcionarios) {
                item.Listar ();
            }
            Console.WriteLine ("APERTE ENTER PARA CONTINUAR...");
            Console.ReadKey ();
            Console.Clear ();

        }
    }
}