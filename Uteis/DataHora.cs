using System;

namespace folhapagamento.Uteis {
    public class DataHora {
        public static int ContaSabadoDomingo (int mes, int ano) {
            DateTimeOffset data = DateTimeOffset.Parse ($"{ano}-{mes}-1");
            int diaFim = data.AddMonths (1).AddDays (-1).Day;
            int qtdSabadosDomingos = 0;
            for (int i = 0; i < diaFim; i++) {
                if ((data.AddDays (i).DayOfWeek.ToString () == "Sunday") || (data.AddDays (i).DayOfWeek.ToString () == "Saturday")) {
                    qtdSabadosDomingos++;
                }
            }
            return qtdSabadosDomingos;
        }
    }
}